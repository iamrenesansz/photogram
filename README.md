A Instagram-like application using Ionic Framework.

## Getting Started
1. Install nodejs: 
> [http://nodejs.org/ ](http://nodejs.org/)

2. Install Ionic, Bower, Cordova and Gulp
> $ npm install -g ionic bower cordova gulp

3. Intall Node Modules
> $ npm install

 or 
> $ sudo npm install

4. Install Bower Libs
> $ bower install

5. Gulp dev tas
> $ gulp  

## Install Cordova Plugins (Required)

Statusbar 
> $ ionic plugin add cordova-plugin-statusbar

Camera
> $ ionic plugin add org.apache.cordova.camera

Image Picker
> $ ionic plugin add https://github.com/wymsee/cordova-imagePicker.git

Geolocation
> $ ionic plugin add org.apache.cordova.geolocation

In App Browser
> $ionic plugin add org.apache.cordova.inappbrowser
> 

## Ionic Analytics (required)
1. Create Account 
>  [Apps Ionic.io](https://apps.ionic.io/)

2. Configure your app name
> ionic.project
> bower.json
> package.json

3. Enter command in your terminal
> $ ionic io init

Finished, Ionic Analytics configured!

Access Ionic Apps Dashboard and view your app analytics report
[https://apps.ionic.io/apps](https://apps.ionic.io/apps)

## Configure Parse 
1. Create account in Parse 
> [http://parse.com](http://parse.com)

2. Edit file and set your Parse Keys
>www/js/config.parse.js

![enter image description here](http://movibe.github.io/photogram-docs/assets/images/facebook-config.jpg)


## Test in Web Browser
After following the above instructions, type in your terminal for run ionic server

> $ ionic serve
  
## Facebook Configuration

 1. Create Account in Facebook Developers
 2. Edit File with your Keys www/js/config.facebook.js

## Build and run app on iOS (only Mac OS)
> $ sudo npm install -g ios-sim 

> $ ionic run ios