(function (window, angular, undefined) {
  'use strict';
  angular
    .module('module.user')
    .controller('IntroCtrl', function (gettextCatalog, $state) {
      var vm = this;

      vm.slides = [{
          heading: 'Welcome to<br />the World of Trapp',
          content: gettextCatalog.getString('In your city, while on your trip, share best of the best Coffee Shops, Restaurants, Hotels and Locations and know where they are worldwide. Don\'t miss the pleasures, get followers and follow others. Get more likes in your travel, upload your pictures and videos. Finally, make the difference and let your happiness make you famous.'),
          features: ['Explore new locations', 'Find people and places', 'Connect with other people Trapp members around the world', 'Share the image you like'],
          footer: 'Discover your place.',
          img: 'img/slide-1.jpg',
          hasLogo: true
        }, {
          bottom: gettextCatalog.getString('Sit back and relax.'),
          img: 'img/slide-2.jpg',
          hasLogo: false
        }, {
          bottom: gettextCatalog.getString('Savor a good food.'),
          img: 'img/slide-3.jpg',
          hasLogo: false
        }, {
          bottom: gettextCatalog.getString('Enjoy a great stay.'),
          img: 'img/slide-4.jpg',
          hasLogo: false
        }, {
          bottom: gettextCatalog.getString('Explore great places.'),
          img: 'img/slide-5.jpg',
          hasLogo: false
        }
      ];

      vm.slideIndex = 0;

      // Called each time the slide changes
      vm.slideChanged = function (index) {

        if (index < vm.slides.length)
            vm.slideIndex = index;
        else
            $state.go('user-signin');

      };

    });
})(window, window.angular);
