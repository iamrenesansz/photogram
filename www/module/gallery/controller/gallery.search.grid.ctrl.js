(function (window, angular, undefined) {

    'use strict';

    angular.module('module.gallery').controller('GallerySearchGridCtrl', GallerySearchGridCtrl);

    function GallerySearchGridCtrl($scope, Gallery, $timeout) {

        var vm = this;
            vm.loading = true;
            vm.search = '';
            vm.contacts = [];

        function _Init() {
            vm.data = [];
            vm.page = 0;
            vm.more = false;
            vm.empty = false;
            vm.SearchUser();
            vm.load('');
        }

        vm.SearchUser = function () {

            var query = new Parse.Query('User');
                query.contains('username', vm.search);
                query.find({
                    success: function (res) {
                        $timeout(function () {

                            angular.forEach(res, function (value) {
                                console.log(value);
                                Gallery
                                    .getUserGalleryQtd(value.id)
                                    .then(function (qtdPhotos) {

                                        value.set('qtdPhotos', qtdPhotos);

                                        vm.contacts = res;

                                    });

                            });

                            // vm.contacts = res;

                        }, 0);
                    },
                    error: function () {
                        $timeout(function () {
                            vm.contacts = [];
                        }, 0);
                    }
                });

        };

        vm.load = function (string) {

            Gallery.search(string, vm.page)
                .then(function (res) {

                    // console.log('Seach people');
                    angular.forEach(res, function (value) {
                        
                        vm.data.push(value);
                    });

                    if (res.length > 0) {
                        vm.more = true;
                        vm.page++;
                    } else {
                        vm.empty = true;
                        vm.more = false;
                    }

                })
                .then(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                })
                .catch(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');

                    if (vm.data.length) {
                        vm.loading = false;
                        vm.page++;
                    } else {
                        vm.empty = true;
                        vm.loading = false;
                    }

                    vm.more = false;

                });

        };

        $scope.loadMore = function (force) {
            console.log('Force Load...');
            vm.load(force);
        };

        _Init();

    }

})(window, window.angular);
