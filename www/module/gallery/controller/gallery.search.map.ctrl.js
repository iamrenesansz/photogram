(function (window, angular, undefined) {

    'use strict';

    angular.module('module.gallery').controller('GallerySearchMapCtrl', function ($scope, $timeout, User, GeoService, Gallery) {

      var vm = this;

      vm.map = {
        center: { latitude: 45, longitude: -73 },
        zoom: 13
      };

      vm.location = function () {
        init();
      };

      var time = 0;

      $scope.$watch('map.center.latitude', function (newValue) {

        if (newValue) {

          time += 2000;

          var timer = $timeout(function () {

            Gallery
              .nearby(vm.map.center)
              .then(function (resp) {
                console.log(resp);
                time = 0;
                vm.data = resp;

                $timeout.cancel(timer);
              });
          }, time);

        }
      });

      vm.openModal = function (item) {
        console.log(item);
      };

      function init() {
        GeoService
          .findMe()
          .then(function (position) {

            console.log(position);

            vm.map = { center: position.geolocation, zoom: 13 };
            vm.user = angular.copy(position.geolocation);

            Gallery
              .nearby(position.coords)
              .then(function (resp) {
                console.log(resp);
                vm.data = resp;
              });

          }, function () {
            console.log('Could not get location');

            Gallery
              .nearby(vm.map.center)
              .then(function (resp) {
                console.log(resp);
                vm.data = resp;
              });
          });
      }

      init();

    });
})(window, window.angular);
