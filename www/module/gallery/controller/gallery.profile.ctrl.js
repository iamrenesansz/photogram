(function (window, angular, undefined) {
  'use strict';
  angular
    .module('module.gallery')
    .controller('GalleryProfileCtrl', function ($stateParams, Gallery, UserForm, User) {

      var vm = this;

      function init() {
        Gallery
          .getUser($stateParams.id)
          .then(function (resp) {
            vm.form = resp;
          });
      }

      init();
    });
})(window, window.angular);
