(function (window, angular, undefined) {
    'use strict';
    angular
        .module('module.gallery')
        .directive('galleryFollowers', function ($ionicModal, $q, Gallery, Loading, $timeout, User) {
            return {
                restrict: 'A',
                scope   : {
                    user: '='
                },
                link    : function (scope, elem, attr) {

                    function init() {
                        var defer            = $q.defer();
                        scope.loadingGallery = true;

                        Gallery
                            .getUserGallery(scope.user.id)
                            .then(function (resp) {
                                scope.data = resp;
                                console.log(resp);
                                scope.$broadcast('scroll.refreshComplete');
                                scope.$broadcast('scroll.infiniteScrollComplete');
                                scope.loadingGallery = false;
                                defer.resolve(scope.data);
                            });

                        return defer.promise;
                    }

                    function getFollower(userId) {
                        scope.loadingGallery = true;
                        scope.followers = [];

                        User
                            .getFollowerProfile(userId)
                            .then(function (qtdFollowers) {
                                console.log('qtdFollower: seguindo');

                                for (var i = 0, li = qtdFollowers.length; i < li; ++i) {
                                    qtdFollowers[i].get('user').fetch({
                                        success: function (res) {
                                            scope.followers.push({
                                                id: res.get('objectId'),
                                                username: res.get('username'),
                                                email: res.get('email'),
                                                name: res.get('name'),
                                                src: res.get('src')
                                            });

                                            scope.loadingGallery  = false;
                                        },
                                        error: function (err) { console.log(err); },
                                    });
                                }

                            });

                    }

                    elem.bind('click', function () {

                        $ionicModal
                            .fromTemplateUrl('module/gallery/view/gallery.followers.modal.html', {
                                scope: scope
                            })
                            .then(function (modal) {
                                scope.modalProfile = modal;
                                scope.modalProfile.show();

                                init();
                                getFollower(scope.user.id);

                                scope.loadingFollow = true;

                                scope.closeModal = function () {
                                    delete scope.data;
                                    scope.modalProfile.hide();
                                    scope.modalProfile.remove();
                                };
                            });
                    });
                }
            }
        });
})(window, window.angular);
