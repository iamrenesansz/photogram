(function (window, angular, undefined) {
  'use strict';
  angular
    .module('ionic')
    .constant('AppConfig', {
      app: {
        name: 'Photogram',
        url: 'http://photogramapp.com',
        image: 'http://photogramapp.com/social-share.jpg',
      },
      routes: {
        home: 'gallery.trending',
        login: 'intro'
      },
      color: '#00796B',
      facebook: '1024016557617380',
      parse: {
        applicationId: 'yAwd3FUBD1Mjqm86RiL8lWSCTzgdOXkpspvn5mCl',
        clientKey: 'xOITM2fQZym1eeQdBzA3u0Np2llymIf8zUFIRfhv',
        javascriptKey: 'FXPpgLGWvIDtz9d61ZT26GoGmMwAmjzljJv2igWp'
      }
    });
})(window, window.angular);
